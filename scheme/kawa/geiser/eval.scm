;;; eval.scm -- evaluation

;; Copyright (C) 2018 Mathieu Lirzin <mthl@gnu.org>
;; Copyright (C) 2019 spellcard199 <spellcard199@protonmail.com>

;; This program is free software; you can redistribute it and/or
;; modify it under the terms of the Modified BSD License. You should
;; have received a copy of the license along with this program. If
;; not, see <http://www.xfree86.org/3.3.6/COPYRIGHT2.html#5>.

;; Stack trace
(define *geiser-related-stack-trace* #t)
(define (geiser:set-geiser-related-stack-trace-visible
         visible ::<boolean>)
  (set! *geiser-related-stack-trace* visible))
;; Pretty-print result
(define *geiser-print-pretty-result* ::<boolean> #t)
(define (geiser:set-print-pretty-result pretty ::<boolean>)
  (set! *geiser-print-pretty-result* pretty))
;; Pretty-print output
(define *geiser-print-pretty-output* ::<boolean> #t)
(define (geiser:set-print-pretty-output pretty ::<boolean>)
  (set! *geiser-print-pretty-output* pretty))

(define (geiser-format-exception-stacktrace
         ex ::java.lang.Exception)
  ::java.lang.String
  "A function that takes a java exception and returns a formatted
string containing:
- exception's message
- exception's stack trace
- exception's cause, if present"

  (let* ((stackTrace ::java.lang.StackTraceElement[]
                     (ex:getStackTrace))
         (st-port (open-output-string)))

    ;; Print exception class and message.
    (display (format "~A: ~A"
                     (*:getName (ex:getClass))
                     (ex:getMessage))
             st-port)
    (newline st-port)

    ;; Print stack trace
    (do ((i 0 (+ 1 i)))
        ((or (equal? "geiser.sld" ((stackTrace i):getFileName))
             (>= i (- (length stackTrace) 1)))
         (if *geiser-related-stack-trace*
             (begin
               (display "\n\t--- " st-port)
               (display "geiser-related part of the stack trace"
                        st-port)
               (display " ---\n\n" st-port)
               (do ((j i (+ 1 j)))
                   ((>= j (- (length stackTrace) 1)))
                 (display (format "\tat ~a\n" (stackTrace j))
                          st-port)))
             (begin
               (display "\n\t# " st-port)
               (display "geiser-related part of stack trace is hidden."
                        st-port)
               (display "\n\t# " st-port)
               (display "To re-enable it use:" st-port)
               (display "\n\t# " st-port)
               (display
                "(geiser:set-geiser-related-stack-trace-visible #t)"
                st-port))))
      (display (format "\tat ~a\n" (stackTrace i))
               st-port))

    ;; Print cause: goes after stack trace and it's not indented.
    (when (not (equal? #!null (ex:getCause)))
      (display "(exception:getCause) : " st-port)
      (display (ex:getCause) st-port)
      (newline st-port))

    (when (not (equal? 0 (ex:getSuppressed):length))
      (newline st-port)
      (display "Suppressed: " st-port)
      (display (ex:getSuppressed) st-port)
      (newline st-port))

    (when (not (equal? (ex:getMessage) (ex:getLocalizedMessage)))
      (newline st-port)
      (display "LocalizedMessage: " st-port)
      (display (ex:getLocalizedMessage) st-port)
      (newline st-port))

    ;; Return formatted exception.
    (st-port:toString)))

(define (call-with-output-and-exceptions-to-string-port
         (port ::gnu.kawa.io.CharArrayOutPort)
         (proc ::<procedure>)
         #!rest proc-args)
  ;; Note: warnings produced:
  ;; - eval-ing in the repl: go to `gnu.kawa.io.OutPort:outDefault'
  ;; - using (load filepath): go to `gnu.kawa.io.OutPort:errDefault'

  (let* (;; Save Scheme's output and error default OutPort objects
         (outDefault ::<output-port> (gnu.kawa.io.OutPort:outDefault))
         (errDefault ::<output-port> (gnu.kawa.io.OutPort:errDefault))

         ;; Save Java's output and error default PrintStream objects
         (save-java-out ::java.io.PrintStream java.lang.System:out)
         (save-java-err ::java.io.PrintStream java.lang.System:err)

         ;; Both Java's and Scheme's output and error eventually go ;;
         ;; to `baos'.
         (baos ::java.io.ByteArrayOutputStream
               (java.io.ByteArrayOutputStream))
         ;; Java writes to `ps', which writes to `baos'.
         (ps   ::java.io.PrintStream
               (java.io.PrintStream baos))
         ;; Scheme writes to `temp-port', which writes to `ps', which
         ;; writes to `baos'.
         (temp-port ::gnu.kawa.io.OutPort
                    (gnu.kawa.io.OutPort
                     (java.io.OutputStreamWriter ps)
                     *geiser-print-pretty-output*
                     #t))
         ;; We are using a `temp-port' instead of using directly the
         ;; passed `port' because we want to preserve the order in
         ;; which output is produced.
         ;; Problem is:
         ;; 1. java.lang.System.setOut and setErr accept only a
         ;;    PrintStream
         ;; 2. There is not a way (that I know of) of setting an
         ;;    OutputStream for an already existing OutPort
         ;; 3. If Java's output went to `ps' and Scheme's output went to
         ;;    `port' the 2 would not be cronologically interspersed as
         ;;    they would during a normal execution
         ;; Solution:
         ;; 1. Everything goes to `baos'
         ;; 2. When finished we copy `baos''s content to `port'
         (result ::java.lang.Object
                 (try-finally
                  (try-catch
                   (begin
                     ;; Redirect scheme output and err
                     (gnu.kawa.io.OutPort:setOutDefault temp-port)
                     (gnu.kawa.io.OutPort:setErrDefault temp-port)
                     ;; Redirect java output and err
                     (java.lang.System:setOut ps)
                     (java.lang.System:setErr ps)
                     ;; return proc result
                     (apply proc proc-args))
                   (ex <java.lang.Exception>
                       (display (geiser-format-exception-stacktrace ex))
                       ;; return 'kawa-exception
                       'kawa-exception))
                  (begin
                    ;; Restore Scheme's default output and error
                    (gnu.kawa.io.OutPort:setOutDefault outDefault)
                    (gnu.kawa.io.OutPort:setErrDefault errDefault)
                    ;; Restore Java's default output and error
                    (java.lang.System:setOut save-java-out)
                    (java.lang.System:setErr save-java-err)
                    (temp-port:flush)
                    (ps:flush)
                    (invoke temp-port 'close)
                    (port:write (baos:toString))
                    (invoke port 'close)))))
    result))

(define (get-output-string-catching-nullpointerexception-bug
         output-port ::gnu.kawa.io.CharArrayOutPort)
  ;; A minimal example to reproduce the NullPointerException can be:
  ;;   (import (kawa expressions))
  ;;   (display (->exp 'foobar))
  ;; On 2019-09-08 this example has been fixed on the kawa master branch.
  (try-catch
   (get-output-string output-port)
   (ex <java.lang.NullPointerException>
       (string-concatenate
        `("####################
 Message from geiser:eval
 - Kawa wasn't able to display the output.
 - NullPointerException was raised when calling `get-output-string' on `output-port', where the evaluation's output is accumulated.
 - This happens only when displaying some objects and (output-port.getPrettyWriter):buffer becomes #!null.
 - It may be a kawa bug, so if/when it becomes fixed in a stable version of kawa this message can be removed.
 - As a workaround you can try using (format \"~A\" culprit) before displaying or writing the 'culprit' object.
 #####################\n\n"
          ,(geiser-format-exception-stacktrace ex))))))

(define (eval-to-string env      ::<gnu.mapping.Environment>
                        code-str ::<java.lang.String>)
  ::<java.lang.String>
  "Returns the string representation of the result of evaluation."
  ;; There are 2 reasons we don't use kawa.standard.Scheme.eval:
  ;; - It doesn't capture warnings
  ;; - As of 2019-11-09 it has an issue with the ! macro.
  ;;   For example, this crashes the JVM:
  ;;     ((kawa.standard.Scheme:getInstance):eval
  ;;       "(! foobar (java.lang.String))")
  ;;     ((kawa.standard.Scheme:getInstance):eval
  ;;       "foobar")
  ;;
  ;; Then there is kawa.Shell.run. Differently from
  ;; kawa.standard.Scheme.eval, kawa.Shell.run does not return the
  ;; actual value, but stores the result's string representation in a
  ;; Consumer or in an OutPort. If we really needed the result value
  ;; we would have to write our own eval-like procedure, but since in
  ;; Geiser we don't need the actual value of the result but just a
  ;; representation of it that we can give to emacs, kawa.Shell.run
  ;; works enough for our purposes.

  (let* ((lang     ::<kawa.standard.Scheme>
                   ((kawa.standard.Scheme):getInstance))
         (env      ::<gnu.mapping.Environment>
                   (interaction-environment))
         (inp      ::<gnu.kawa.io.CharArrayInPort>
                   (gnu.kawa.io.CharArrayInPort code-str))

         (swout  ::<java.io.StringWriter>
                 (java.io.StringWriter))
         (pout ::<gnu.kawa.io.OutPort>
               (gnu.kawa.io.OutPort
                swout *geiser-print-pretty-result* #t))

         (swerr ::<java.io.StringWriter>
                (java.io.StringWriter))
         (perr ::<gnu.kawa.io.OutPort>
               (gnu.kawa.io.OutPort
                ;; pretty: #f no need to pretty print.
                swerr #f #t))

         (messages ::<gnu.text.SourceMessages>
                   (gnu.text.SourceMessages))

         ;; Actually eval
         (throwable ::<java.lang.Throwable>
                    (kawa.Shell:run
                     lang env inp pout perr messages))

         (msgs-to-print ::<java.lang.String>
                        (messages:toString 50000))
         ;; Close result and err ports.
         (_ ::<void> (begin
                       (perr:flush) (perr:close)
                       (pout:flush) (pout:close)))
         (err-str ::<java.lang.String>
                  (let ((err-str ::<java.lang.String>
                                 (swerr:toString)))
                    (if (> (err-str:length) 0)
                        err-str
                        #!null)))
         (res-str ::<java.lang.String>
                  (swout:toString)))

    (when msgs-to-print
      (display msgs-to-print))
    (when err-str
      (display err-str))
    (when throwable
      ;; If a throwable is raised during evaluation do not return,
      ;; raise it. We leave the handling to the caller.
      (throw throwable))
    res-str))

(define (geiser-eval module ::gnu.mapping.Environment
                     str    ::<string>
                     #!rest rest)

  ;; In this comment it's explained why this procedure:
  ;; - takes a string instead of a quoted sexp, like other geiser
  ;;   scheme implementations
  ;; - uses the custom eval function `eval-to-string' instead of
  ;;   kawa's builtin eval

  ;; Q. Why does this procedure take a string instead of a sexp?
  ;; A. Kawa has an issue with using colon notation after (this) when
  ;; defining classes. If you do it, the whole sexp becomes unquotable
  ;; so the form wouldn't be able to even arrive here without it being
  ;; evaluated and raising an exception.

  ;; Q. Why are we using eval-to-string instead of plain (eval ...)?
  ;; A. 2 reasons:
  ;; - Consistency of behavior between evaluation in an emacs repl
  ;;   buffer and in an emacs geiser buffer
  ;; - To capture warnings

  ;; About consistency of behavior: if geiser:eval used only scheme's
  ;; builtin `eval', trying evaluate (define-library (foobar)) inside
  ;; a geiser-buffer would raise an exception, because it would call
  ;; `geiser:eval', while typing it in the repl buffer would not,
  ;; because it would be like calling:
  ;;   ((kawa.standard.Scheme:getInstance):eval ...)

  ;; About capturing warnings: I wasn't able to find a way to capture
  ;; warnings with neither:
  ;; - (eval ...)
  ;; - ((kawa.standard.Scheme:getInstance):eval ...)

  (let* ((module-is-interaction-env?
          ::boolean
          (equal? module (interaction-environment)))
         (output-port ::gnu.kawa.io.CharArrayOutPort
                      (open-output-string))
         ;; Actually eval
         (result-str-or-exception-sym
          ;; `result-str-or-exception-sym' can be either:
          ;; - result representation as <java.lang.String>
          ;; - 'kawa-exception
          (call-with-output-and-exceptions-to-string-port
           output-port eval-to-string module str))
         (eval-raised-exception? ::boolean
                                 (equal?
                                  result-str-or-exception-sym
                                  'kawa-exception))
         ;; Get output
         (output-str
          ;; Temporary workaround for what seems a kawa bug when
          ;; displaying certain objects. When it's fixed it can be
          ;; replaced with just (get-output-string output-port).
          (get-output-string-catching-nullpointerexception-bug
           output-port)))

    (format "~S" (list
                  (if eval-raised-exception?
                      `(error  ,result-str-or-exception-sym)
                      `(result ,result-str-or-exception-sym))
                  `(output . ,output-str)))))

;; To see warnings from geiser:load, for example, we need to wrap
;; output in the geiser protocol and therefore to call geiser:eval.
;; However, in the default kawa language (the r7rs kawa variant
;; doesn't have this issue) colon has special meaning, similar to
;; java's dot, so it cannot call geiser:eval from the inside of the
;; library because here geiser is not a class.
;; Here we define a second name for geiser-eval, so that:
;; - geiser-eval: is the private definition used inside the `geiser'
;;   library
;; - geiser:eval: is another name meant just to be exported, to keep
;;   the same naming convention other geiser scheme implementations use
;; We could approach the problem using different solutions, but this
;; is the simplest and works for now.
(define geiser:eval geiser-eval)

(define (geiser:load-file filepath)
  (geiser-eval (interaction-environment)
               (format "~S" `(load ,filepath))))
