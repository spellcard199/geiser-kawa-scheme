;;; completions.scm -- completion support

;; Copyright (C) 2018 Mathieu Lirzin <mthl@gnu.org>
;; Copyright (C) 2019 spellcard199 <spellcard199@protonmail.com>

;; This program is free software; you can redistribute it and/or
;; modify it under the terms of the Modified BSD License. You should
;; have received a copy of the license along with this program. If
;; not, see <http://www.xfree86.org/3.3.6/COPYRIGHT2.html#5>.

(define (geiser:completions prefix
                            #!optional
                            (module (interaction-environment))
                            #!rest rest)
  "Return a list of possible symbol completions matching prefix in
`module' or (interaction-environment), if `module' is `nil'.

`module' can be either:
- a gnu.mapping.Environment
- a scheme module identifier (a proper list)"

  (let* ((sieve (if (string=? prefix "")
                    cons
                    (lambda (symb acc)
                      (if (string-contains (symbol->string symb) prefix)
                          (cons symb acc)
                          acc))))
         (env ::gnu.mapping.Environment
              (cond
               ;; Already an environment
               ((*:isAssignableFrom gnu.mapping.Environment
                                    (invoke module 'getClass))
                module)
               ;; Module specifier -> environment
               ((list? module)
                (environment module))
               ;; `module' has wrong type
               (else (throw
                      (java.lang.RuntimeException
                       "`module''s type should be either a proper list or a gnu.mapping.Environment.")))))

         (lst (environment-fold env sieve '()))

         (symbol-ci<? (lambda (s1 s2)
                        (string-ci<? (symbol->string s1)
                                     (symbol->string s2)))))
    ;; Return sorted list of matches
    (sort lst symbol-ci<?)))

(define (base-location->module-name
         location ::gnu.mapping.Location)
  ;; TODO: if possible, rewrite this without resorting to hacky string
  ;; manipulations.

  ;; We want to get:
  ;; 1. from: gnu.mapping.PlainLocation[some.thing.here]
  ;; 2. to  : "(some thing here)"
  ;; Instead of PlainLocation we may also have:
  ;; - gnu.kawa.reflect.FieldLocation
  ;; - gnu.mapping.SharedLocation

  ;; It sounds strange to me that there isn't a better way, but I
  ;; wasn't able to find a different way than resorting to string
  ;; manipulation.

  (let* ((loc-as-str (location:toString))

         ;; from: "gnu.mapping.PlainLocation[some.thing.here]"
         ;; to  : "gnu.mapping.PlainLocation[some.thing.here"
         (removing-trailing-square-bracket
          ::java.lang.String
          (loc-as-str:substring 0 (- (loc-as-str:length) 1)))

         ;; from: "gnu.mapping.PlainLocation[some.thing.here"
         ;; to  : "some.thing.here"
         (removing-leading-location-txt
          ::java.lang.String
          (let* ((splitted
                  (removing-trailing-square-bracket:split "\\[")))
            ;; get the 2nd element:
            ;; the first one should be "...Location"
            ((java.util.Arrays:copyOfRange
              splitted 1 splitted:length)
             0)))

         ;; from: "some.thing.here"
         ;; to  : ("some" "thing" "here")
         ;; "here" is the symbol, but we want just the "module"
         ;; containing it (which in kawa is a class under the hood).
         (module-str-lst
          (let* ((splitted ::java.lang.String[]
                           (removing-leading-location-txt:split "\\.")))
            (apply list (java.util.Arrays:copyOfRange
                         splitted 0 (- splitted:length 1)))))

         ;; from: ("some" "thing" "here")
         ;; to  : "(some thing here)"
         (module-str-repr
          (let ((out-str (open-output-string)))
            (display module-str-lst out-str)
            (get-output-string out-str))))

    module-str-repr))

(define (geiser:module-completions prefix)
  (let ((completions ::java.util.ArrayList (java.util.ArrayList)))
    ;; Since this procedure works iterating over locations in the
    ;; (interaction-environment), if a module does not export any
    ;; symbol it won't appear in the result.
    ;; TODO: this is an hack. If it exists, find a way to list
    ;; modules directly.
    (((interaction-environment
       ):enumerateAllLocations
        ):forEachRemaining
         (lambda (loc ::gnu.mapping.Location)
           (let ((module-str-repr ::string
                                  (base-location->module-name
                                   (loc:getBase))))
             ;; module-str-repr should be like: "(... ... ...)"
             (when (and
                    (not (completions:contains module-str-repr))
                    (not (string-null? module-str-repr))
                    ;; TODO: Is there anything we want to hide?
                    ;; (not (string-prefix? "(gnu" module-str-repr))
                    (string-prefix? prefix module-str-repr))
               (completions:add module-str-repr)))))

    ;; Geiser protocol wants modules in the result to be printed
    ;; between double quotes
    ;; ("(... ... ...)" "(... ...)")
    ;; Kawa repl doesn't show returned strings with surrounding
    ;; quotes, so we have to manually surround completions.
    (map
     (lambda (m) (string-concatenate (list "\"" m "\"")))
     completions)))
