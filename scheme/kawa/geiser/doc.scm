;;; doc.scm -- procedures providing documentation on scheme objects

;; Copyright (C) 2019 spellcard199 <spellcard199@protonmail.com>

;; This program is free software; you can redistribute it and/or
;; modify it under the terms of the Modified BSD License. You should
;; have received a copy of the license along with this program. If
;; not, see <http://www.xfree86.org/3.3.6/COPYRIGHT2.html#5>.

(define (geiser:manual-epub-unzip-to-tmp-dir
         kawa-manual-epub-path ::<java.lang.String>)
  (let* ((system-tmpdir ::<java.lang.String>
                        (java.lang.System:getProperty
                         "java.io.tmpdir"))
         (manual-unzipped-dest-dir ::<java.lang.String>
                                   (java.lang.String:join
                                    java.io.File:separator
                                    system-tmpdir
                                    "geiser-kawa"
                                    "manual-epub-unzipped")))
    (unzip-file kawa-manual-epub-path
                manual-unzipped-dest-dir)
    (format "~S" manual-unzipped-dest-dir)))
