;;; utils.scm -- utility functions

;; Copyright (C) 2019 spellcard199 <spellcard199@protonmail.com>

;; This program is free software; you can redistribute it and/or
;; modify it under the terms of the Modified BSD License. You should
;; have received a copy of the license along with this program. If
;; not, see <http://www.xfree86.org/3.3.6/COPYRIGHT2.html#5>.

(define (unzip-file archive  ::<java.lang.String>
                    dest-dir ::<java.lang.String>)
  (try-catch
   (let* ((zip-file ::<java.util.zip.ZipFile>
                    (java.util.zip.ZipFile archive))
          (enu ::<java.util.Enumeration>
               (zip-file:entries))
          (dest-dir-name ::<java.lang.String>
                         (if (dest-dir:endsWith java.io.File:separator)
                             dest-dir
                             (dest-dir:concat java.io.File:separator))))
     (do ((zip-entry ::<java.util.zip.ZipEntry>
                     (enu:nextElement)
                     (enu:nextElement)))
         ((not (enu:hasMoreElements)))
       (let* ((name ::<java.lang.String>
                    (dest-dir-name:concat
                     (zip-entry:getName)))
              (size ::<long>
                    (zip-entry:getSize))
              (compressed-size ::<long>
                               (zip-entry:getCompressedSize))
              (file ::<java.io.File>
                    (java.io.File name)))
         (if (name:endsWith "/")
             (file:mkdirs)
             (let ((_ ::<void>
                      (when (file:getParentFile)
                        ((file:getParentFile):mkdirs)))
                   (is ::<java.io.InputStream>
                       (zip-file:getInputStream
                        zip-entry))
                   (fos ::<java.io.FileOutputStream>
                        (java.io.FileOutputStream file))
                   (bytes ::<byte[]>
                          (byte[] length: 1024)))
               (do ((length ::<java.lang.Integer>
                            (is:read bytes)
                            (is:read bytes)))
                   ((< length 0))
                 (fos:write bytes 0 length))
               (is:close)
               (fos:close)))))
     (zip-file:close)
     dest-dir-name)
   (ex <java.io.IOException>
       (ex:printStackTrace))))
