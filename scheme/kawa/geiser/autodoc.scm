;;; geiser.scm -- support for autodoc echo

;; Copyright (C) 2019 spellcard199 <spellcard199@protonmail.com>

;; This program is free software; you can redistribute it and/or
;; modify it under the terms of the Modified BSD License. You should
;; have received a copy of the license along with this program. If
;; not, see <http://www.xfree86.org/3.3.6/COPYRIGHT2.html#5>.

(import (only (srfi 1) iota drop take))

(define *geiser-autodoc-show-types* ::<boolean> #t)
(define (geiser:set-autodoc-show-types show-types ::<boolean>)
  (set! *geiser-autodoc-show-types* show-types))


;;;; Utility procedures : may be moved somewhere else in future

(define (reflect-get-declared-field
         obj        ::<java.lang.Object>
         field-name ::<string>)
  ;; Boilerplate to access java private fields
  ;; Works for both:
  ;; - objects and instance fields
  ;; - classes and static fields
  (let* ((static-field? ::<boolean>
                        (equal? java.lang.Class obj:class))
         (field-reflect ::<java.lang.reflect.Field>
                        (*:getDeclaredField
                         (if static-field?
                             (as java.lang.Class obj)
                             obj:class)
                         field-name))
         (field-access-save ::<boolean>
                            (field-reflect:isAccessible))
         (field-value       #!null))
    (try-finally
     (begin
       (field-reflect:setAccessible #t)
       (if static-field? 
           (set! field-value (field-reflect:get #!null))
           (set! field-value (field-reflect:get obj))))
     ;; finally clause: leave field access as we found it
     (field-reflect:setAccessible field-access-save))
    field-value))


;;;; TODO: autodoc for macros


;;;; autodoc for procedures

(define (class->gb-class-type c ::<java.lang.Class>)
  ::<gnu.bytecode.ClassType>
  ;; For us, the relevent difference between this and
  ;; gnu.bytecode.ClassType:make is that the latter doesn't populate
  ;; the `code' field in the ClassType object.
  (let* ((class-loader ::<java.lang.ClassLoader>
                       (let ((cl ::<java.lang.ClassLoader>
                                 (*:getClassLoader c)))
                         (if cl cl (invoke-static
                                    java.lang.ClassLoader
                                    'getSystemClassLoader))))
         (resource-path ::<java.lang.String>
                        (invoke (invoke (*:getName c)
                                        'replace
                                        "." "/")
                                'concat
                                ".class"))
         (resource-is ::<java.io.InputStream>
                      (let ((ris (class-loader:getResourceAsStream
                                  resource-path)))
                        (if ris
                            ris
                            (throw
                             (java.lang.RuntimeException
                              (invoke
                               (java.lang.String "missing resource ")
                               'concat
                               resource-path))))))
         (class-type ::<gnu.bytecode.ClassType>
                     (gnu.bytecode.ClassType)))
    ;; Effectful: modifies fields in `class-type'.
    (gnu.bytecode.ClassFileInput class-type resource-is)
    class-type))

(define (prim-procedure->gb-method
         proc ::<gnu.expr.PrimProcedure>)
  ::<gnu.bytecode.Method>

  ;; Each PrimProcedure corresponds to only 1 method.
  ;; When a primitive java method has more than one signature Kawa
  ;; maps it to gnu.expr.GenericProc, which is not a concern of this
  ;; procedure.

  ;; The reason for which just doing (proc:getMethod) doesn't work is
  ;; that we need a ClassType object that has a `code' field
  ;; populated. `gnu.bytecode.ClassFileInput' in
  ;; `class->gb-class-type' does that, but it means we have to find
  ;; the Method again in the newly made ClassType object.

  (let* ((method-name ::<java.lang.String>
                      ((proc:getMethod):getName))
         (param-types ::<gnu.bytecode.Type[]>
                      ((proc:getMethod):getParameterTypes))
         ;; Populate ClassType fields
         (class-type ::<gnu.bytecode.ClassType>
                     (class->gb-class-type
                      (*:getReflectClass
                       (proc:getDeclaringClass))))
         (search-classes
          ::java.util.ArrayList[gnu.bytecode.ClassType]
          (do ((search-classes
                ::java.util.ArrayList[gnu.bytecode.ClassType]
                (java.util.ArrayList))
               (cur-class ::<gnu.bytecode.ClassType>
                          class-type
                          (*:getSuperclass cur-class)))
              ((equal? cur-class
                       #!null)
               search-classes)
            (search-classes:add cur-class)))
         ;; Search also in superclasses
         (gbmethod ::<gnu.bytecode.Method>
                   (do ((gbm ::<gnu.bytecode.Method> #!null)
                        (i 0 (+ 1 i)))
                       (gbm
                        ;; If the method is declared by a superclass
                        ;; we need to populate ClassType fields for
                        ;; the newly found class and then re-get the
                        ;; method.
                        (let ((dclass ::<gnu.bytecode.ClassType>
                                      (gbm:getDeclaringClass)))
                          (if (equal? dclass class-type)
                              gbm
                              (*:getMethod
                               (class->gb-class-type
                                (*:getReflectClass dclass))
                               method-name
                               param-types))))
                     (try-catch
                      (set! gbm
                        (*:getMethod (as gnu.bytecode.ClassType
                                         (search-classes i))
                                     method-name
                                     param-types))
                      (ex <java.lang.NullPointerException>
                          #!void)))))
    gbmethod))

(define (compiled-proc->gb-methods
         proc ::<gnu.expr.CompiledProc>)
  ::<gnu.bytecode.Method[]>
  ;; Differently from PrimProcedure, a CompiledProc has to wrap
  ;; potentially different signatures, to support scheme procedures.
  (let* ((lu   ::<java.lang.invoke.MethodHandles$Lookup>
               (java.lang.invoke.MethodHandles:lookup))
         (ifmn ::<java.lang.invoke.MethodHandleInfo>
               (lu:revealDirect (field proc 'applyToObjectMethod)))
         (owner-class ::<java.lang.Class>
                      (ifmn:getDeclaringClass))
         (check-method-name ::<java.lang.String>
                            (ifmn:getName))
         ;; We need the actual method, not the "...$check" one
         (method-name ::<java.lang.String>
                      (check-method-name:substring
                       0
                       (- (check-method-name:length) 6)))
         (class-type ::<gnu.bytecode.ClassType>
                     ;; This does not work for us, because it does not
                     ;; populate the code field
                     ;; (gnu.bytecode.ClassType:make
                     ;;   (invoke owner-class 'getName))
                     (class->gb-class-type owner-class))
         (possible-names ::<list>
                         (list method-name
                               (method-name:concat "$V")
                               (method-name:concat "$X")
                               (method-name:concat "$V$X")))
         (methods-matching-name
          ::java.util.ArrayList[gnu.bytecode.Method]
          (do ((m ::<gnu.bytecode.Method>
                  (*:getMethods class-type)
                  (m:getNext))
               (methods-found ::java.util.ArrayList[gnu.bytecode.Method]
                              (java.util.ArrayList)))
              ((equal? m #!null) methods-found)
            (when (member (m:getName) possible-names)
              (methods-found:add m)))))

    (methods-matching-name:toArray (gnu.bytecode.Method[]))))

(define (generic-proc->MethodProcs
         proc ::<gnu.expr.GenericProc>)
  ::<gnu.mapping.MethodProc[]>
  (let ((methods ::java.util.ArrayList[gnu.mapping.MethodProc]
                 (java.util.ArrayList)))
    (do ((i 0 (+ 1 i)))
        ((>= i (proc:getMethodCount)))
      (methods:add (proc:getMethod i)))
    (apply gnu.mapping.MethodProc[] methods)))

(define (gb-method->local-var-attr
         m ::<gnu.bytecode.Method>)
  (do ((lva ::<gnu.bytecode.LocalVarsAttr> #!null)
       (cattr ::<gnu.bytecode.Attribute>
              ((m:getCode):getAttributes)
              (cattr:getNext)))
      ((or (equal? #!null cattr)
           lva)
       lva)
    (when (equal? gnu.bytecode.LocalVarsAttr
                  (*:getClass cattr))
      (set! lva cattr))))

(define (gb-local-var-attr->local-vars
         lva ::<gnu.bytecode.LocalVarsAttr>)
  ::<gnu.bytecode.Variable[]>
  (let* ((vars ::java.util.ArrayList[gnu.bytecode.Variable]
               (java.util.ArrayList))
         (stop ::<boolean> #f)
         (var  ::<gnu.bytecode.Variable>
               (let ((all-vars ::<gnu.bytecode.VarEnumerator>
                               (lva:allVars)))
                 (if (all-vars:hasMoreElements)
                     (all-vars:nextElement)
                     (begin (set! stop #t)
                            #!null)))))
    (do ()
        (stop (apply gnu.bytecode.Variable[] vars))
      (vars:add var)
      (if (var:hasMoreElements)
          (set! var (var:nextElement))
          (set! stop #t)))))

(define (gb-method->maybe-local-vars
         m ::<gnu.bytecode.Method>)
  (let ((lva ::<gnu.bytecode.LocalVarsAttr>
             (gb-method->local-var-attr m)))
    (if lva
        (gb-local-var-attr->local-vars lva)
        #!null)))

(define (gb-var->start-pos
         var ::<gnu.bytecode.Variable>)
  (let* ((scope ::<gnu.bytecode.Scope>
                (var:getScope))
         (start-label ::<gnu.bytecode.Label>
                      (when scope
                        (reflect-get-declared-field
                         scope "start")))
         (start-position ::<int>
                         (when start-label
                           (reflect-get-declared-field
                            start-label "position"))))
    start-position))

(define (gb-method->maybe-param-names
         m ::<gnu.bytecode.Method>)
  ::<java.lang.String[]>
  ;; TODO: only works for static methods. Is it possible to generalize
  ;; to work with instance methods? Probably yes, but I can't do it.
  (let ((param-types ::<gnu.bytecode.Type[]>
                     (m:getParameterTypes))
        (local-vars  ::<gnu.bytecode.Variable[]>
                     (gb-method->maybe-local-vars m))
        (param-names ::<java.lang.String[]>
                     #!null))

    (when local-vars
      (let* ((i ::<java.lang.Integer> 0)
             (var ::<gnu.bytecode.Variable>
                  (local-vars i))
             (stop ::<boolean> #f)
             (possible-param-names
              ::java.util.ArrayList[java.lang.String]
              (java.util.ArrayList)))
        ;; TODO: I can't make the bindings in do loops to work. Using
        ;; outer let instead
        (do ()
            ((or stop
                 (>= i param-types:length)
                 (>= i local-vars:length)))
          (set! var (local-vars i))
          (let* (;; (var-offset    ::<int> var:offset) ;; unused
                 (var-name      ::<java.lang.String> (var:getName))
                 (var-type      ::<gnu.bytecode.Type> (var:getType))
                 (var-start-pos ::<int>
                                (gb-var->start-pos var))
                 (param-type-at-index ::<gnu.bytecode.Type>
                                      (param-types i)))
            (if (and
                 ;; Again, only works for static methods.
                 (not (string=? "this" var-name))
                 ;; Again, only works for static methods.
                 (equal? var-start-pos 0)
                 (< i param-types:length)
                 (equal? param-type-at-index
                         var-type))
                (possible-param-names:add var-name)
                (set! stop #t)))
          (set! i (+ 1 i)))

        (when (equal? (possible-param-names:size)
                      param-types:length)
          (set! param-names
            (apply java.lang.String[] possible-param-names)))))

    param-names))

(define (gb-method-get-data m ::<gnu.bytecode.Method>)
  (let* ((method-name        ::<java.lang.String>
                             (m:getName))
         (method-modifiers   ::<java.lang.Integer>
                             (m:getModifiers))
         (method-return-type ::<gnu.bytecode.Type>
                             (m:getReturnType))
         (param-types        ::<gnu.bytecode.Type[]>
                             (m:getParameterTypes))
         (fallback-param-names ::<java.lang.String[]>
                               ;; Returns: (arg0 arg1 ...)
                               (apply
                                java.lang.String[]
                                (map (lambda (i ::<int>)
                                       (format "arg~s" i))
                                     (iota param-types:length))))
         ;; The rest of the procedure is just to try to get actual
         ;; argument names out of java bytecode to use instead of
         ;; `fallback-param-names'. Kawa (awesomely) includes an API
         ;; that does exactly that. If we don't find names that
         ;; satisfy our criteria we discard the result and use just
         ;; `fallback-param-names'.
         (method-param-names-found ::<boolean> #f)
         (param-names ::<java.lang.String[]> ;; May be mutated below
                      (let ((maybe-bytecode-param-names
                             ::<java.lang.String[]>
                             (gb-method->maybe-param-names m)))

                        (if maybe-bytecode-param-names
                            (begin
                              (set! method-param-names-found #t)
                              maybe-bytecode-param-names)
                            fallback-param-names)))
         ;; `method-params' should be an association list:
         ;; `((pname: ,param-name) (ptype: ,param-type)
         ;;   ...)
         ;; We already have param-types. If we can't find pnames
         ;; in the java bytecode we are defaulting to (arg0 arg1 ...)
         (method-params ::<list>
                        (map (lambda ((name ::<java.lang.String>)
                                      (type ::<gnu.bytecode.Type>))
                               `((pname: ,name)
                                 (ptype: ,type)))
                             param-names
                             param-types)))
    `((method-name:              ,method-name)
      (method-modifiers:         ,method-modifiers)
      (method-param-names-found: ,method-param-names-found)
      (method-params:            ,method-params)
      (method-return-type:       ,method-return-type))))

(define (format-param
         param ::<list>
         #!optional
         (optional? ::<boolean> #f))
  ;; string or symbol or list depending on *geiser-autodoc-show-types*
  ;; and optional?
  (let* ((pname ::<java.lang.String>
                (cadr (assoc 'pname: param)))
         (ptype ::<gnu.bytecode.Type>
                (cadr (assoc 'ptype: param)))
         (formatted-type
          (format "::~a" (*:getName (*:getReflectClass ptype))))
         (read-str (lambda (x ::<java.lang.String>)
                     (read (open-input-string x)))))
    (cond ((and *geiser-autodoc-show-types*       (not optional?))
           (format "~a~a" pname formatted-type))
          ((and *geiser-autodoc-show-types*       optional?)
           (format "(~a~a)" pname formatted-type))
          ((and (not *geiser-autodoc-show-types*) (not optional?))
           (read-str (format "~a" pname)))
          ((and (not *geiser-autodoc-show-types*) optional?)
           (read-str (format "(~a)" pname))))))

(define (prim-procedure->autodoc-arglist
         p ::<gnu.expr.PrimProcedure>)
  (let* ((method-data (gb-method-get-data
                       (prim-procedure->gb-method p)))
         (method-params
          (cadr (assoc 'method-params: method-data))))
    ;; Wrapping in additional list to maintain consistency between
    ;; nesting of GenericProc, CompiledProc, PrimiProcedure.
    `((,(cons "required"
              (map (lambda (param)
                     (format-param param))
                   method-params))))))

(define (compiled-proc->autodoc-arglist
         p ::<gnu.expr.CompiledProc>)
  (let* ((methods-data (map gb-method-get-data
                            (compiled-proc->gb-methods p)))
         (method-with-param-names
          (let ((mwpn #!null))
            (for-each
             (lambda (m)
               (when (cadr (assoc 'method-param-names-found: m))
                 (set! mwpn m)))
             methods-data)
            mwpn))
         (method-with-most-params
          (let ((mwmp #!null)
                (max-params 0))
            (for-each
             (lambda (m)
               (let ((l (length (cadr (assoc 'method-params: m)))))
                 (when (> l max-params)
                   (set! mwmp m)
                   (set! max-params l))))
             methods-data)
            mwmp))
         ;; TODO: I need to know if this can happen.
         ;; If it does, logic should be changed.
         (_ ::<void> (when (and
                            method-with-param-names
                            (not (equal?
                                  method-with-most-params
                                  method-with-param-names)))
                       (throw
                        (java.lang.Exception
                         "[compiled-proc->autodoc-data] \
                          method-with-most-params \
                          != method-with-param-names"))))
         ;; TODO?
         (_ ::<void> (when (string-suffix?
                            "$V%X"
                            (cadr (assoc 'method-name:
                                         method-with-most-params)))
                       (throw
                        (java.lang.Exception
                         "[compiled-proc->autodoc-data] TODO"))))
         (method-with-least-params
          (let ((mwlp #!null)
                (least-params 9999))
            (for-each
             (lambda (m)
               (let ((l (length (cadr (assoc 'method-params: m)))))
                 (when (< l least-params)
                   (set! mwlp m)
                   (set! least-params l))))
             methods-data)
            mwlp))
         (all-params (cadr (assoc 'method-params:
                                  method-with-most-params)))
         ;; `slice' is slighlty more manageable than `split-at'
         ;; https://stackoverflow.com/questions/108169/how-do-i-take-a-slice-of-a-list-a-sublist-in-scheme
         (slice (lambda (l offset ::<integer> n ::<integer>)
                  (take (drop l offset) n)))
         (required-par-num (length
                            (cadr (assoc
                                   'method-params:
                                   method-with-least-params))))
         (optional-params? ::<boolean>
                           (not (equal? (length all-params)
                                        required-par-num)))
         (variadic? (string-suffix? "$V"
                                    (cadr
                                     (assoc 'method-name:
                                            method-with-most-params))))
         (required-params ::<list>
                          (if optional-params?
                              (slice all-params
                                     0
                                     required-par-num)
                              (if variadic?
                                  (slice all-params
                                         0
                                         (- (length all-params) 1))
                                  all-params)))
         (optional-params ::<list>
                          (if optional-params?
                              (slice
                               all-params
                               required-par-num
                               (let ((par-num (length all-params)))
                                 (if variadic?
                                     (- par-num required-par-num 1)
                                     (- par-num required-par-num 0))))
                              '()))
         (rest-param (if variadic?
                         (list-ref all-params
                                   (- (length all-params) 1))
                         #!null)))
    ;; Wrapping in a list so that the procedure for autodoc-arglist
    ;; for CompiledProc, PrimProcedure and GenericProc have the same
    ;; nesting level.
    (list
     ;; Very ugly. Actually the whole procedure is very ugly.
     (append
      (if (> (length required-params) 0)
          `(("required" ,@(map (lambda (p)
                                 (format-param p))
                               required-params)))
          '())
      (if (or optional-params? variadic?)
          `(("optional"
             ,@(append
                (if optional-params?
                    (map (lambda (p)
                           (format-param p #t))
                         optional-params)
                    '())
                (if variadic?
                    (list (format
                           "(... ~a...)"
                           (format-param rest-param)))
                    '()))))
          '())))))

(define (generic-proc->autodoc-arglist
         p ::<gnu.expr.GenericProc>)
  (let* ((method-procs ::<gnu.mapping.MethodProc[]>
                       (generic-proc->MethodProcs p))
         (arglist (map method-proc->arglist
                       method-procs)))
    (apply append arglist)))

(define (method-proc->arglist
         mp ::<gnu.mapping.MethodProc>)
  (cond ((equal? gnu.expr.CompiledProc
                 (*:getClass mp))
         (compiled-proc->autodoc-arglist mp))
        ((equal? gnu.expr.PrimProcedure
                 (*:getClass mp))
         (prim-procedure->autodoc-arglist mp))
        ((equal? gnu.expr.GenericProc
                 (*:getClass mp))
         (generic-proc->autodoc-arglist mp))))

(define (named-part->autodoc-arglist
         ;; p ::<gnu.kawa.functions.NamedPart>
         p)
  ;; Type annotation for p crashes kawa. Using explicit check instead.
  (when (not (equal? (*:getClass p) gnu.kawa.functions.NamedPart))
    (error (format "[named-part->autodoc-arglist] ~s \
                    cannot be cast to gnu.kawa.functions.NamedPart"
                   (*:getClass p))))
  (let ((method-proc ::<gnu.mapping.MethodProc>
                     (reflect-get-declared-field p "methods")))
    (method-proc->arglist method-proc)))


;;;; Dispatch based on operator type
(define (geiser-operator-arglist op)
  (cond ((equal? gnu.expr.CompiledProc
                 (*:getClass op))
         (compiled-proc->autodoc-arglist op))
        ((equal? gnu.expr.PrimProcedure
                 (*:getClass op))
         (prim-procedure->autodoc-arglist op))
        ((equal? gnu.expr.GenericProc
                 (*:getClass op))
         (generic-proc->autodoc-arglist op))
        ((equal? gnu.kawa.functions.NamedPart
                 (*:getClass op))
         (named-part->autodoc-arglist op))
        (else `((("required" ,(format "autodoc-unsupported: ~a"
                                      (*:getClass op))))))))


;;;; Entry point
(define (geiser:autodoc ids #!rest rest)
  ;; TODO: find the "right" way to get modules for symbols.
  ;; TODO: support for procedures defined in java, like `append'
  ;; TODO: support for macros (possible?)
  ;; TODO: support for getting parameter names for java instance
  ;;       methods from bytecode with ClassType (not so simple)
  ;; TODO: support names with special characters, like |a[)|
  ;;       Maybe we can:
  ;;       1. keep a list of special chars
  ;;       2. when `id' contains one: surround with || (e.g. |id|)

  ;; At the moment arguments are enclosed in double quotes. The
  ;; reason is that geiser's output is `read' by elisp, but java types
  ;; may contain characters that are not valid in elisp symbols
  ;; (e.g. java arrays contain square brackets). So instead of symbols
  ;; we use strings. When type annotations are disabled using
  ;; (geiser:set-autodoc-show-types #f) parameter names displayed as
  ;; symbols (without double quotes around them).

  (map
   (lambda (id ::<symbol>)
     (let ((operator
            (try-catch
             (eval id)
             (ex <gnu.mapping.UnboundLocationException>
                 #!null))))
       (if (not operator)
           #f
           `(,id
             ,(format
               "~S"
               (cons "args"
                     (if (not (symbol? id))
                         #f
                         (geiser-operator-arglist operator))))
             ("\"module\""
              ;; TODO : write a procedure that gets the module from
              ;; which a symbol comes in the right way (is there one?).
              ,@(if ;; If it's a CompiledProc it's easy
                 (equal? (*:getClass operator) gnu.expr.CompiledProc)
                 (let* ((comp-proc ::<gnu.expr.CompiledProc>
                                   operator)
                        (module ::<java.lang.Class>
                            (comp-proc:getModule)))
                   (string-split (*:getName module) "."))
                 ;; If it's not a CompiledProc it does not have a
                 ;; `getModule' method: fallback to trying to figure
                 ;; out from Location in Environment.
                 (try-catch
                  (read
                   (open-input-string
                    (base-location->module-name
                     (((interaction-environment
                        ):lookup id):getBase))))
                  ;; If it is not even a sym in
                  ;; (interaction-environment) give up.
                  ;; TODO: should we consider all java classes as modules?
                  (ex <java.lang.NullPointerException>
                      '()))))))))
   ids))

;;; Local Variables:
;;; geiser-scheme-implementation: kawa
;;; End:

;;; autodoc.scm ends here.
