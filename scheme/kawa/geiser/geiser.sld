;; Copyright © 2019 spellcard199 <spellcard199@protonmail.com>

;; This program is free software; you can redistribute it and/or
;; modify it under the terms of the Modified BSD License. You should
;; have received a copy of the license along with this program. If
;; not, see <http://www.xfree86.org/3.3.6/COPYRIGHT2.html#5>.

(define-library (geiser)
  (export

   ;; eval.scm
   geiser:set-geiser-related-stack-trace-visible
   geiser:eval
   geiser:load-file
   geiser:set-print-pretty-result
   geiser:set-print-pretty-output

   ;;completions.scm
   geiser:completions
   geiser:module-completions

   ;; autodoc.scm
   geiser:autodoc
   geiser:set-autodoc-show-types

   ;; doc.scm
   geiser:manual-epub-unzip-to-tmp-dir

   geiser:no-values
   geiser:newline)

  (import (kawa base)
          (only (srfi 1) delete-duplicates filter)
          (only (srfi 95) sort))

  (begin
    (include "eval.scm")
    (include "completions.scm")
    (include "autodoc.scm")
    (include "utils.scm")
    (include "doc.scm")

    (define (geiser:no-values)
      ;; This is probably a hack, but seems to work.
      ;; Compared to just using #!void or (values), which in a repl
      ;; buffer, when sending an empty input would...
      ;;   - show double prompt
      ;;   - increase line count by 2
      ;; ... this behaves more like a normal kawa repl:
      ;;   - shows a single prompt
      ;;   - increases line count just by 1
      ((gnu.kawa.io.InPort:inDefault):setLineNumber
       (- ((gnu.kawa.io.InPort:inDefault):getLineNumber)
          1)))

    (define (geiser:newline)
      #f)))
